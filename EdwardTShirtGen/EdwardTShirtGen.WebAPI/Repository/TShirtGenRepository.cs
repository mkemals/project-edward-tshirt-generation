﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using Dapper;

namespace EdwardTShirtGen.WebAPI.Repository
{
    public static class TShirtGenRepository
    {

        public static Models.TShirtGenResponse AddTShirtGenerationLog(Models.TShirtGen model)
        {
            Models.TShirtGenResponse res = new Models.TShirtGenResponse();
            try
            {
                string ConnStrEdward = ConfigurationManager.ConnectionStrings["ConnStrEdward"].ConnectionString;
                using (var conn = new SqlConnection(ConnStrEdward))
                {
                    res = conn.Query<Models.TShirtGenResponse>("EXEC Edward.uspAddTShirtGen @Color, @Name, @BirthYear, @ClientIP, @UserAgent ", model).FirstOrDefault() ?? new Models.TShirtGenResponse();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.AddToFile(ex.ToString());
            }
            return res;
        }
        public static int AddTShirtGenerationView(Models.TShirtGenView model)
        {
            int res = 0;
            try
            {
                string ConnStrEdward = ConfigurationManager.ConnectionStrings["ConnStrEdward"].ConnectionString;
                using (var conn = new SqlConnection(ConnStrEdward))
                {
                    res = conn.Execute("INSERT INTO Edward.TShirtGenView(Alias, Referrer, ViewDate) VALUES(@Alias, @Referrer, @ViewDate)", model);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.AddToFile(ex.ToString());
            }
            return res;
        }

        public static Models.TShirtGen GetTShirtGenerationLog(Guid alias)
        {
            Models.TShirtGen res = new Models.TShirtGen();
            try
            {
                string ConnStrEdward = ConfigurationManager.ConnectionStrings["ConnStrEdward"].ConnectionString;
                using (var conn = new SqlConnection(ConnStrEdward))
                {
                    res = conn.Query<Models.TShirtGen>("SELECT * FROM Edward.TShirtGen WHERE Alias=@Alias", new { Alias = alias }).FirstOrDefault() ?? new Models.TShirtGen();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.AddToFile(ex.ToString());
            }
            return res;
        }


    }
}
