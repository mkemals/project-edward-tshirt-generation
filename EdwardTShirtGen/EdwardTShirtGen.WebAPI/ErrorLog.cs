﻿using System;
using System.IO;

namespace EdwardTShirtGen.WebAPI
{
    public class ErrorLog
    {
        public static void AddToFile(string contents, string seperateFileName = "")
        {
            try
            {
                string logDir = AppDomain.CurrentDomain.BaseDirectory + "\\logs\\";
                Directory.CreateDirectory(logDir);
                if (seperateFileName != "") seperateFileName = "_" + seperateFileName;
                string fname = logDir + DateTime.Now.ToString("yyyy-MM-dd") + seperateFileName + ".txt";
                using (StreamWriter sw = new StreamWriter(fname, true))
                {
                    sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " : " + contents);
                }
            }
            catch
            {

            }
        }
    }
}
