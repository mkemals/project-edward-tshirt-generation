﻿using System;

namespace EdwardTShirtGen.WebAPI.Models
{
    public class TShirtGenView
    {
        public Guid Alias { get; set; } = Guid.Empty;
        public string Referrer { get; set; }
        public DateTime ViewDate { get; set; } = DateTime.Now;
    }
}