﻿namespace EdwardTShirtGen.WebAPI.Models
{
    public class ColorBox
    {
        public string ColorName { get; set; }
        public bool DarkFont { get; set; } = true;
    }
}