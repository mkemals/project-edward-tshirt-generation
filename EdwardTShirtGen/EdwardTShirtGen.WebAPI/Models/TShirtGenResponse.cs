﻿using System;

namespace EdwardTShirtGen.WebAPI.Models
{
    public class TShirtGenResponse
    {
        public int TShirtGenCOUNT { get; set; } = 0;
        public Guid InsertedAlias { get; set; } = Guid.Empty;
    }
}
