﻿using System;

namespace EdwardTShirtGen.WebAPI.Models
{
    public class TShirtGen
    {
        public int Id { get; set; } = 0;
        public Guid Alias { get; set; } = Guid.Empty;
        public string Color { get; set; } = "";
        public string Name { get; set; } = "";
        public string BirthYear { get; set; } = "";
        public string ClientIP { get; set; } = "";
        public string UserAgent { get; set; } = "";
        public DateTime CreatedDate { get; set; } = DateTime.Now;
    }
}