﻿using System;
using System.Linq;
using System.Web;

namespace EdwardTShirtGen.WebAPI
{
    public static class IPHelper
    {
        public static string GetClientIpAddress(HttpContext context, string headerName = "HTTP_CLIENT_IP")
        {
            var ip = String.Empty;
            if (context.Request.ServerVariables.AllKeys.Contains(headerName))
                ip = context.Request.ServerVariables[headerName];
            else if (context.Request.ServerVariables.AllKeys.Contains("X-Real-Ip"))
                ip = context.Request.ServerVariables["X-Real-Ip"];
            else
                ip = "-";
            return ip;
        }
    }
}
