﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using EdwardTShirtGen.WebAPI.Models;

namespace EdwardTShirtGen.WebAPI.Controllers
{

    [EnableCors("*", "*", "*")]
    //[RoutePrefix("api/edwardtshirt")]
    [RoutePrefix("api/tshirt")]
    public class TShirtController : ApiController
    {

        /// <summary>
        /// Dictionary
        ///             Key: Renk Adı
        ///           Value: Font rengi koyu mu olacak?
        /// </summary>
        private List<ColorBox> ColorBoxWhiteList = new List<ColorBox>
        {
            new ColorBox{ ColorName = "beyaz", DarkFont =  true },
            new ColorBox{ ColorName = "acikgri", DarkFont =  true },
            new ColorBox{ ColorName = "koyugri", DarkFont =  false },
            new ColorBox{ ColorName = "siyah", DarkFont =  false },
            new ColorBox{ ColorName = "kirmizi", DarkFont =  true },
            new ColorBox{ ColorName = "acikmavi", DarkFont = true },
            new ColorBox{ ColorName = "yesil", DarkFont =  true },
            new ColorBox{ ColorName = "turuncu", DarkFont =  true },
            new ColorBox{ ColorName = "mor", DarkFont =  true },
            new ColorBox{ ColorName = "sari", DarkFont =  true },
            new ColorBox{ ColorName = "eflatun", DarkFont =  true },
            new ColorBox{ ColorName = "pembe", DarkFont =  true },
        };

        [HttpGet]
        [Route("v1/colors")]
        public List<ColorBox> GetColorList()
        {
            return ColorBoxWhiteList;
        }

        [HttpGet]
        [Route("v1/box/{color}")]
        public HttpResponseMessage GetBox(string color)
        {
            if (!ColorBoxWhiteList.Any(f => f.ColorName == color))
                color = "beyaz";
            string filePath = System.Web.HttpContext.Current.Request.MapPath($"~\\Assets\\kutular\\{color}.png");
            Image img = Image.FromFile(filePath);
            MemoryStream ms = new MemoryStream();
            img.Save(ms, ImageFormat.Png);
            ms.Position = 0;
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(ms);
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/png");
            return response;
        }

        [HttpGet]
        [Route("v1/draft/{color}")]
        public HttpResponseMessage GetDraft(string color)
        {
            if (!ColorBoxWhiteList.Any(f => f.ColorName == color))
                color = "beyaz";
            string filePath = System.Web.HttpContext.Current.Request.MapPath($"~\\Assets\\renkler\\{color}.png");
            Image img = Image.FromFile(filePath);
            MemoryStream ms = new MemoryStream();
            img.Save(ms, ImageFormat.Png);
            ms.Position = 0;
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(ms);
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/png");
            return response;
        }

        [HttpGet]
        [Route("v1/create/{color}")]
        public string Save(string color) { return Save(color, "", ""); }
        [HttpGet]
        [Route("v1/create/{year}/{color}")]
        public string Save(string color, string year) { return Save(color, "", year); }
        [HttpGet]
        [Route("v1/create/{name}/{year}/{color}")]
        public string Save(string color, string name, string year)
        {
            #region " ---- Validation ---- "
            //for Color
            if (!ColorBoxWhiteList.Any(f => f.ColorName == color))
                color = "beyaz";
            //for Name
            name = name.Length > 14 ? name.Substring(0, 14) : name;
            //for Year
            int y;
            if (int.TryParse(year, out y))
            {
                if (y < 1900 || y > 2019)
                    year = "";
            }
            else
            {
                year = "";
            }
            #endregion
            #region " ---- Loglama ---- "
            string agent = Request.Headers.UserAgent.ToString();
            string ip = IPHelper.GetClientIpAddress(System.Web.HttpContext.Current);
            TShirtGenResponse res = Repository.TShirtGenRepository.AddTShirtGenerationLog(new TShirtGen
            {
                Color = color,
                Name = name,
                BirthYear = year,
                ClientIP = ip,
                UserAgent = agent,
            });
            #endregion
            return res.InsertedAlias.ToString("N");
        }

        [HttpGet]
        [Route("v1/design/{alias}")]
        public HttpResponseMessage GetSavedDesign(string alias)
        {
            try
            {
                TShirtGen model = Repository.TShirtGenRepository.GetTShirtGenerationLog(alias.ConvGuid());
                #region " ---- View Log ---- "
                string referrer = Request.Headers.Referrer?.ToString();
                Repository.TShirtGenRepository.AddTShirtGenerationView(new TShirtGenView
                {
                    Alias = model.Alias,
                    Referrer = referrer,
                    ViewDate = DateTime.Now
                });
                #endregion
                return GetDesign(GetDesign(model));
            }
            catch (Exception ex)
            {
                ErrorLog.AddToFile(ex.ToString());
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/png");
                return response;
            }
        }

        [HttpGet]
        [Route("v1/design64/{alias}")]
        public string GetSavedDesignAsBase64(string alias)
        {
            try
            {
                TShirtGen model = Repository.TShirtGenRepository.GetTShirtGenerationLog(alias.ConvGuid());
                return GetBase64Image(GetDesign(model));
            }
            catch (Exception ex)
            {
                ErrorLog.AddToFile(ex.ToString());
                return "";
            }
        }

        public HttpResponseMessage GetDesign(MemoryStream ms)
        {
            try
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StreamContent(ms);
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/png");
                return response;
            }
            catch (Exception ex)
            {
                ErrorLog.AddToFile(ex.ToString());
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/png");
                return response;
            }
        }
        public MemoryStream GetDesign(TShirtGen model)
        {
            MemoryStream ms = new MemoryStream();
            try
            {
                #region " ---- Validation ---- "
                //for Color
                if (!ColorBoxWhiteList.Any(f => f.ColorName == model.Color))
                    model.Color = "beyaz";
                //for Name
                model.Name = model.Name.Length > 14 ? model.Name.Substring(0, 14) : model.Name;
                //for Year
                int y;
                if (int.TryParse(model.BirthYear, out y))
                {
                    if (y < 1900 || y > 2019)
                        model.BirthYear = "";
                }
                else
                {
                    model.BirthYear = "";
                }
                #endregion
                string filePath = System.Web.HttpContext.Current.Request.MapPath($"~\\Assets\\renkler\\{model.Color}.png");
                Image img = Image.FromFile(filePath);
                Brush brush = new SolidBrush(Color.FromArgb(191, 191, 191));
                ColorBox cBox = ColorBoxWhiteList.First(f => f.ColorName == model.Color);
                if (cBox.DarkFont)
                {
                    brush = new SolidBrush(Color.FromArgb(64, 64, 64));
                }
                Graphics gpx = Graphics.FromImage(img);
                #region " ---- Doğum Yılı ---- "
                Font fontForYear = new Font(FontFamily.GenericSerif, 32f, FontStyle.Bold, GraphicsUnit.Pixel);
                PointF pointOfYear = new PointF(162f, 143f);
                gpx.DrawString(model.BirthYear, fontForYear, brush, pointOfYear);
                #endregion
                #region " ---- İsim ---- "
                float emSize_ForName = 12f;
                if (model.Name.Length < 15) emSize_ForName = 12f;
                if (model.Name.Length < 12) emSize_ForName = 14f;
                if (model.Name.Length < 10) emSize_ForName = 16f;
                Font fontForName = new Font(FontFamily.GenericSerif, emSize_ForName, FontStyle.Bold, GraphicsUnit.Pixel);
                PointF pointOfName = new PointF(360f, -210f);
                gpx.RotateTransform(38);
                gpx.DrawString(model.Name, fontForName, brush, pointOfName);
                #endregion
                img.Save(ms, ImageFormat.Png);
                ms.Position = 0;
            }
            catch (Exception ex)
            {
            }
            return ms;
        }

        public string GetBase64Image(MemoryStream ms)
        {
            return Convert.ToBase64String(ms.ToArray());
        }

    }
}
